document.querySelector('#btnColorStyle').onclick = () => {
    if (localStorage['style'] === 'dark') {
        document.getElementById('stylepage').setAttribute('href', './css/grey.css');
        localStorage['style'] = 'grey';
    } else {
        document.getElementById('stylepage').setAttribute('href', './css/dark.css');
        localStorage['style'] = 'dark';
    }
}

if (localStorage.getItem('style') === null || localStorage.getItem('style') === 'grey'){
    document.getElementById('stylepage').setAttribute('href', './css/grey.css')
    localStorage['style'] = "grey";
}else {
    document.getElementById('stylepage').setAttribute('href', './css/dark.css')
}